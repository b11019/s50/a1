const coursesData = [
    {
        id: "wdc001",
        title: "Web Development Course",
        description: "This is a web development course",
        price: "45000",
        onOffer: true,
        img: "https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
    },
    {
        id: "wdc002",
        title: "Python-Django Course",
        description: "This is a python-django course",
        price: "35000",
        onOffer: true,
        img: "https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
    },
    {
        id: "wdc003",
        title: "Java-Spring Course",
        description: "This is a java-spring course",
        price: "85000",
        onOffer: true,
        img: "https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
    }
];
export default coursesData;