import {useEffect, useState} from 'react';
import { Card } from "react-bootstrap";
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';


export default function CourseCard({courseProp}) {
    console.log(courseProp);
    // console.log(typeof courseProp);

    
    const {name,description, price, _id} = courseProp
    
     

    

    // console.log(`Enrollees: ${count}`);

    return (
        
        <Card className="cardCourse p-3">
        <Card.Body>
            <Card.Title>
            {name}
            </Card.Title>
            <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
            <Card.Text>
                {/* lorem ipsum dolor sit amet consectetur adipisicing elit. */} {description}
            </Card.Text>
            <Card.Text className="price mb-0">
                Price: 
            </Card.Text>
            <Card.Text className="peso">
                {price}
            </Card.Text>
           
            <Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
            
            
        </Card.Body>
        </Card>
      
    );
}
// export default CourseCard;