import { Button,Row, Col } from "react-bootstrap";


export default function Banner() {
  return (
   <Row>
        <Col className = "p-5">
            <h1>Booking App-182</h1>
            <p> Enroll in our courses and get access to our online resources.</p>
            <Button variant="primary">Enroll Now!</Button>
        </Col>
   </Row>
    );
}

// export default Banner;