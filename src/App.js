
import './App.css';
import {useState, useEffect} from 'react';
import AppNavBar from './components/AppNavBar';
import Container from 'react-bootstrap/Container';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext';


function App() {
  const [user, setUser] = useState({
    email: null,
    id : null
  });
  
  const unsetUser = () => {
    localStorage.clear();
  }
  useEffect(() => {
    
    fetch('http://localhost:4000/users/getUserDetails', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if (typeof data._id !== 'undefined') {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    } else {
      setUser({
        id: null,
        isAdmin: null
      })
    }
    })

  }, [])
  return (

<UserProvider value={{ user, setUser, unsetUser }}>
<Router>
    <AppNavBar />
    <Container>
      <Routes>
          <Route exact path='/' element={<Home/>}/>
          <Route exact path='/courses' element={<Courses/>}/>
          <Route exact path='/courseView/:courseId' element={<CourseView/>}/>
          <Route exact path='/register' element={<Register/>}/>
          <Route exact path='/login' element={<Login/>}/>
          <Route path='/logout' element={<Logout/>}/>
          <Route path='*' element={<Error/>}/>
      </Routes>
      
    </Container>
 
</Router>
</UserProvider>

  );
}

export default App;
