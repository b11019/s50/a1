import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";



export default function Logout(props) {
    const { setUser, unsetUser} = useContext(UserContext);
    // To clear local storage infos
    unsetUser();

    useEffect(() => {
        setUser ({
            id: null,
            isAdmin: null
        });
    }, [setUser]);


    return (
        <Navigate to="/login" />
    );
}