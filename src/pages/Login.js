import { useEffect } from "react";
import { useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from '../UserContext';
import {Form, Button} from "react-bootstrap";
import  Swal  from 'sweetalert2';



export default function Login(props) {
    console.log(props);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);
    const {user, setUser} = useContext(UserContext);
    console.log(user);

    useEffect(() => {
        if (email  !== '' && password !== '') {
            setIsActive(true);
            
        } else {
            setIsActive(false);
            
        }
        
    }
        , [email, password]);
    
        function loginUser (e) {
            e.preventDefault();

            fetch("http://localhost:4000/users/login", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    if (typeof data.accessToken !== 'undefined') {
                        localStorage.setItem("token", data.accessToken);
                        retrieveUserDetails(data.accessToken)

                        Swal.fire({
                            title: 'Login Successful',
                            icon: 'success',
                            text: 'Welcome to Booking App of 182!',
                            
                        })
                    } else {
                        Swal.fire({
                            title: 'Invalid Credentials',
                            icon: 'error',
                            text: 'Please check your password and email',
                    })
                       
                }
            })
            // localStorage.setItem('email', email)

        //    setUser({
        //     email : localStorage.getItem('email' )
        // });
            setEmail("");
            setPassword("");
           

        }
        
        const retrieveUserDetails = (token) => {
            fetch("http://localhost:4000/users/getUserDetails", {
                method : "POST",
                headers: {
                   
                    "Authorization": `Bearer ${token}`
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    setUser({
                        _id: data._id,
                        isAdmin: data.isAdmin,
                        });
                })
                .catch(err => {
                    console.log(err);
                }
                );
        }
       


    return (
        (user.id !== null) ?
        <Navigate to="/courses" />
        :
        <>
            <h1 className="my-3">Login Here:</h1>
            <Form onSubmit={e => loginUser(e)}>
                <Form.Group controlId="email2" className="mt-3">
                    <Form.Label> Email Address </Form.Label>
                    <Form.Control type='email'
                    placeholder="Please input your email here"
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    />
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>
                <Form.Group controlId="password2">
                    <Form.Label> Password </Form.Label>
                    <Form.Control type='password'
                    placeholder="Please input your password here"
                    required
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    />

                </Form.Group>
               { isActive ? 
                <Button variant="secondary" type="submit" className="my-3">Login</Button>
               :
                <Button variant="danger" type="submit" disabled className="my-3">
                    Login
                </Button>
                }
            </Form>
        </>

    )

}