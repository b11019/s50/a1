import {Row, Col, Button} from "react-bootstrap";
import { Link } from "react-router-dom";



export default function ErrorBanner() {
    return (
     <Row>
          <Col className = "p-5">
              <h1>ERROR 404</h1>
              <p>The page you are looking for is unavailable</p>
              <Button variant="primary" as={Link} to="/">Back to Homepage</Button>
          </Col>
     </Row>
      );
  }